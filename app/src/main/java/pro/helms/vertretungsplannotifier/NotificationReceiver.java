package pro.helms.vertretungsplannotifier;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.FirebaseApp;
import com.google.firebase.crash.FirebaseCrash;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import pro.helms.vertretungsplannotifier.task.LoginTask;
import pro.helms.vertretungsplannotifier.task.PageTask;
import pro.helms.vertretungsplannotifier.task.PageTaskCompleteListener;
import pro.helms.vertretungsplannotifier.util.FileHandling;

/**
 * @author Joshua Helms
 * @datetime 23.7.17
 */

public class NotificationReceiver extends BroadcastReceiver implements PageTaskCompleteListener<Document> {
    private Context curCon;
    @Override
    public void onReceive(Context context, Intent intent) {
        curCon = context; //Context mainly for receiving App File Directory
        if(isNetworkAvailable(context)) {
            startThread(context);
        }
    }

    public static boolean isNetworkAvailable(Context context) {

        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();

    }

    private void startThread(Context c) {
        File sID = new File(c.getFilesDir(), "/sessionID.txt");
        FirebaseApp.initializeApp(curCon);
        if (sID.exists() && !sID.isDirectory()) {
            String phpSession[] = FileHandling.load(sID); //read sessionID from File
            if(phpSession.length == 2) {
                PageTask curPlan = new PageTask(NotificationReceiver.this);
                curPlan.execute(phpSession[0], phpSession[1]); //simple pagetask to fetch the plan
            }
        }
    }

    private ArrayList<String> parseTable(Document plan) { //helper method to parse retrieved html table
        File f = new File(curCon.getFilesDir(),"kurzel.txt");
        ArrayList<String> entfall = new ArrayList<>();
        String lehrerK[];

        if(f.exists() && !f.isDirectory()) {
            lehrerK = FileHandling.load(f);
        }else{
            entfall.add("Bitte lege die Lehrer in den Einstellungen fest, für die du informiert werden möchtest");
            return entfall;
        }



        Element table = plan.select("table").get(1); //get html table
        Elements rows = table.select("tr"); //get all table rows

        for (int i = 1; i < rows.size(); i++) { //loop through and check if any teachers are affected
            Element row = rows.get(i);
            Elements cols = row.select("td");
            for (String aLehrerK : lehrerK) {
                if (cols.get(2).text().contains(aLehrerK)) {
                    if (cols.get(6).text().contains("Entfall"))
                        entfall.add(aLehrerK + " fällt am " + cols.get(0).text() + " in der " + cols.get(1).text() + " Stunde aus.");
                    if (cols.get(6).text().contains("Raumwechsel"))
                        entfall.add("Der Unterricht von " + aLehrerK + " in der " + cols.get(1).text() + " Stunde, findet am " + cols.get(0).text() + " im Raum " + cols.get(5).text() + " statt.");
                }
            }
        }
        return entfall; //return ArrayList with results
    }

    private void createNotification(Context context, String msg, String msgText, String msgAlert){
        //Helper Func to build a Notification
        PendingIntent notiIntent = PendingIntent.getActivity(context, 0,
                new Intent(context, MainActivity.class), 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context) //notification properties
                .setSmallIcon(R.drawable.gag_reiter)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msgText))
                .setContentTitle(msg)
                .setTicker(msgAlert)
                .setContentText(msgText);

        mBuilder.setContentIntent(notiIntent);

        mBuilder.setDefaults(NotificationCompat.DEFAULT_VIBRATE);

        mBuilder.setAutoCancel(true);

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(1, mBuilder.build());
    }


    @Override
    public void onPageTaskFinish(Document output) { //overwritten callback func from pagetask
        File lastTable = new File(curCon.getFilesDir() + "/lastTable.txt"); //init old table
        ArrayList<String> curPlan; //current table(already parsed)
        String fOutput[];
        StringBuilder tmp = new StringBuilder();
        boolean old = false;

        if(output.baseUri().contains("login")){ //Login again if session id was invalid
                File data = new File(curCon.getFilesDir(), "/uData.txt");
                if (data.exists() && !data.isDirectory()) {
                    String uData[] = FileHandling.load(data);
                    LoginTask loginRefresh = new LoginTask(curCon);
                    try {
                        loginRefresh.execute(uData[0], uData[1]).get(); //*.get() to pause thread till task is finished
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                        FirebaseCrash.report(e);
                    }
                    startThread(curCon);
                }
        }else {

            curPlan = parseTable(output); //if session id was valid parse current table ...


            if (lastTable.exists() && !lastTable.isDirectory()) {
                fOutput = FileHandling.load(lastTable);

                for (int i = 0; i < curPlan.size(); i++) {
                    for (int j = 0; j < fOutput.length; j++) { //iterate through old table and check if lines are old
                        if (fOutput[j].equals(curPlan.get(i))) {
                            old = true; //if line the same, then old
                        }
                    }
                    if(!old) { //if not old
                        tmp.append(curPlan.get(i)).append("\n"); //add to our notification
                    }else{
                        old = false; //otherwise change bool back and iterate again
                    }
                }
                if (tmp.length() > 0) {
                    createNotification(curCon, "Neuer Ausfall!", tmp.toString(), "Ausfall"); //in that event create a new notification
                    FileHandling.saveFile(lastTable, curPlan.toArray(new String[curPlan.size()])); //save new file
                }else{
                    FileHandling.saveFile(lastTable, curPlan.toArray(new String[curPlan.size()])); //save the current file
                }
            } else {
                String savF[] = new String[curPlan.size()]; //on first execute just save current plan to file
                for (int i = 0; i < curPlan.size(); i++) {
                    savF[i] = curPlan.get(i);
                    tmp.append(curPlan.get(i)).append("\n");
                }
                createNotification(curCon, "Momentaner Ausfall", tmp.toString(), "Ausfall");
                FileHandling.saveFile(lastTable, savF);
            }
        }
        }
    }

