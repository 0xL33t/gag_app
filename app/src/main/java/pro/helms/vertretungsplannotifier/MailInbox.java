package pro.helms.vertretungsplannotifier;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.File;

import pro.helms.vertretungsplannotifier.task.FetchMailTask;
import pro.helms.vertretungsplannotifier.util.Constants;
import pro.helms.vertretungsplannotifier.util.FileHandling;
import pro.helms.vertretungsplannotifier.util.InboxAdapter;

/**
 * @author Joshua Helms
 * @datetime 23.7.17
 */

public class MailInbox extends AppCompatActivity {
    public InboxAdapter inboxAdapter;
    public ProgressBar loading;
    public ListView mails;
    public View loadMoreView;
    public TextView inetAlert;
    public String[] lData = new String[2]; //loaded Data
    boolean initialised = false; //prevent starting mail service without userdata

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail_inbox);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        loading = (ProgressBar) findViewById(R.id.loadingBar);
        mails = (ListView) findViewById(R.id.mails);
        inetAlert = (TextView) findViewById(R.id.internetAlert);

        setSupportActionBar(toolbar);

        if(getIntent().hasExtra(Constants.TAG_ACTIVITY_FROM)) {
            if (getIntent().getStringExtra(Constants.TAG_ACTIVITY_FROM).equals("FromLogin")) {
                lData[0] = getIntent().getStringExtra(Constants.TAG_USER);
                lData[1] = getIntent().getStringExtra(Constants.TAG_PW);
                initialised = true;
            } else {
                File uData = new File(getBaseContext().getFilesDir(), "/uData.txt");
                if (uData.exists()) {
                    lData = FileHandling.load(uData);
                    initialised = true;
                } else {
                    Intent i = new Intent(MailInbox.this, LoginAct.class);
                    i.putExtra(Constants.TAG_ACTIVITY_FROM, "FromMailFetcher");
                    startActivity(i);
                    finish();
                }
            }
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MailInbox.this,MailActivity.class);
                startActivity(i);
            }
        });

        if(initialised && NotificationReceiver.isNetworkAvailable(getBaseContext())) {
            FetchMailTask receiver = new FetchMailTask(MailInbox.this);
            receiver.execute(lData[0], lData[1]);
        }else{
            inetAlert.setText("Kein Internet verfügbar " + PlanAct.getEmojiByUnicode(0x1F622));
            inetAlert.setVisibility(View.VISIBLE);
        }

        loadMoreView = ((LayoutInflater)this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.loadmore, null, false);

    }

}
