package pro.helms.vertretungsplannotifier;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import pro.helms.vertretungsplannotifier.util.Constants;
import pro.helms.vertretungsplannotifier.util.FileHandling;

/**
 * @author Joshua Helms
 * @datetime 23.7.17
 */

public class MainActivity extends AppCompatActivity{

    boolean isCalled = false; //If List in Settings is changed, it is gonna be saved to file
    boolean clear; //Used in Settings to determine if List is empty
    boolean inSettings; //to determine if Settings layout is active
    private static final int TIME_INTERVAL = 2500; //# milliseconds , desired time passed between two back presses
    private long mBackPressed; //# milliseconds of last time back was pressed
    ImageButton vPlan, settings, back, mail;
    Switch notifications;
    Button add;
    ListView lView; //List with Lehrerkürzel
    ArrayAdapter<String> adapter; //adapter and list to use the list view
    ArrayList<String> list;
    EditText kurzel;
    File fKurzel, fSettings; //#1 List with saved Lehrerkürzel, #2 Setting File


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inSettings = false;

            fSettings = new File(getApplicationContext().getFilesDir(), "/settings.txt"); //loading current settings


                if (fSettings.exists() && !fSettings.isDirectory()) { //checking if settings are available
                    String tmp[];
                    tmp = FileHandling.load(fSettings);
                    if (tmp.length > 0) {  //checks integrity of settings
                        tmp = tmp[0].split(":");
                        if (tmp[0].equals("notifications") && tmp[1].equals("true")) {
                            initNotifier(); //load notification service if notifications are enabled
                        }
                    }
                }


        vPlan = (ImageButton) findViewById(R.id.vPlan); //initializing main menu
        settings = (ImageButton) findViewById(R.id.settings);
        mail = (ImageButton) findViewById(R.id.mail);

        vPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //Vertretungsplan Button
                Intent i = new Intent(MainActivity.this, PlanAct.class); //start plan activity
                i.putExtra(Constants.TAG_ACTIVITY_FROM,"FromMain"); //add sender to info
                startActivity(i);
            }
        });

        mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, MailInbox.class);
                i.putExtra(Constants.TAG_ACTIVITY_FROM,"FromMain");
                startActivity(i);
            }
        });

        settings.setOnClickListener(new View.OnClickListener() { //Settings Button
            @Override
            public void onClick(View v) {
                fKurzel = new File(getApplicationContext().getFilesDir(), "/kurzel.txt");

                setContentView(R.layout.activity_setting); //setting main activity from menu layout to settings layout
                inSettings = true;
                lView = (ListView) findViewById(R.id.kList); //init layout
                kurzel = (EditText) findViewById(R.id.kurzel);
                add = (Button) findViewById(R.id.add);
                back = (ImageButton) findViewById(R.id.back);
                notifications = (Switch) findViewById(R.id.notifications);

                if (fSettings.exists() && !fSettings.isDirectory()) { //again using settings     Note: Need to improve this
                    String tmp[] = FileHandling.load(fSettings);
                    if (tmp.length > 0) {
                        tmp = tmp[0].split(":");
                        if (tmp[0].equals("notifications") && tmp[1].equals("true")) {
                            notifications.setChecked(true);
                        }else
                            notifications.setChecked(false);
                    }
                }


                if(fKurzel.exists() && !fKurzel.isDirectory()){ //init list
                    String tmp[] = FileHandling.load(fKurzel);
                    list = new ArrayList<>(Arrays.asList(tmp));
                    clear = false;
                }else {
                    String tmp[] = {"", "", "", "", ""}; //if no list is available set up an empty list
                    list = new ArrayList<>(Arrays.asList(tmp));
                    clear = true;
                }


                kurzel.setOnEditorActionListener(new TextView.OnEditorActionListener() { //Using Enter as alternative add button
                    int count = 0;
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        boolean handled = false;
                        if(actionId == EditorInfo.IME_ACTION_DONE){
                            setKurzel(count++,kurzel.getText().toString());
                            kurzel.setText("");
                            handled = true;
                        }
                        return handled;
                    }
                });


                adapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_list_item_1,list);
                lView.setAdapter(adapter);

                add.setOnClickListener(new View.OnClickListener() {
                    int temp = 0;
                    @Override
                    public void onClick(View v) {
                        setKurzel(temp++,kurzel.getText().toString());
                        kurzel.setText("");
                        }
                });

                lView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) { //Deleting list item on click
                        Toast.makeText(MainActivity.this,list.get(position) + " gelöscht", Toast.LENGTH_LONG).show();
                        list.remove(position);
                        adapter.notifyDataSetChanged();
                        isCalled = true;
                    }
                });

                back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) { //reset activity
                        Intent intent = getIntent();
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        overridePendingTransition(0, 0);
                        finish();
                        startActivity(intent);
                    }
                });

            }
        });


    }

    @Override
    protected void onDestroy() {
        if(notifications != null) { //failsafe
            if (notifications.isChecked()) {
                String tmp[] = {"notifications:true"};
                FileHandling.saveFile(fSettings, tmp);
            } else {
                String tmp[] = {"notifications:false"};
                FileHandling.saveFile(fSettings, tmp);
            }
        }

        if(isCalled){ //if list changed, save to kürzel file
            String tmp[] = new String[list.size()];
            tmp = list.toArray(tmp);
            FileHandling.saveFile(fKurzel,tmp);
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() { //press twice to destroy activity
        if(mBackPressed + TIME_INTERVAL > System.currentTimeMillis()){
            super.onBackPressed();
            return;
        }else {
            Toast.makeText(this, "Erneut drücken zum Schließen", Toast.LENGTH_SHORT).show();
            if(inSettings) {
                Intent intent = getIntent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                overridePendingTransition(0, 0);
                finish();
                startActivity(intent);
            }
    }

        mBackPressed = System.currentTimeMillis();

    }

    private void initNotifier(){ //helper to initialize notification service

        Intent intent = new Intent(this, NotificationReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(this, 2, intent, 0);
        AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
        long l = new Date().getTime();
        if (l < new Date().getTime()) {
            l += 1200000; // start in 20 mins
        }
        am.setRepeating(AlarmManager.RTC_WAKEUP, l, 1200000, sender); // 3600000 for hour, 1200000 for 20 mins


        /* DEBUG
        Long alertTime = System.currentTimeMillis() +5*1000;

        Intent alertIntent = new Intent(this, NotificationReceiver.class);

        AlarmManager alarmManager = (AlarmManager)
                getSystemService(Context.ALARM_SERVICE);

        alarmManager.set(AlarmManager.RTC_WAKEUP, alertTime,
                PendingIntent.getBroadcast(this, 1, alertIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT));
        */
    }


    private void setKurzel(int clickCount,String addK) { //helper func to edit list
        if (addK.equals("")) {
            Toast.makeText(MainActivity.this, "Field empty!", Toast.LENGTH_SHORT).show();
        } else if (list.contains(addK.toUpperCase())) {
            Toast.makeText(MainActivity.this, "Kürzel bereits gespeichert!", Toast.LENGTH_SHORT).show();
        } else if (addK.length() > 2 || addK.length() < 1) { //DEBUG --> wieder auf 2 ändern
            Toast.makeText(MainActivity.this, "Kürzel kann nicht länger bzw. kürzer als zwei Buchstaben sein!", Toast.LENGTH_LONG).show();
        } else {
            if (clickCount == 0 && clear) list.clear(); //clear the list from empty strings
            list.add(addK.toUpperCase());
            adapter.notifyDataSetChanged();
            isCalled = true;
        }
    }
}



