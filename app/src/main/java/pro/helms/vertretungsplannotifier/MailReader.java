package pro.helms.vertretungsplannotifier;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.File;

import pro.helms.vertretungsplannotifier.task.FetchSingleMailTask;
import pro.helms.vertretungsplannotifier.util.Constants;
import pro.helms.vertretungsplannotifier.util.FileHandling;

public class MailReader extends AppCompatActivity {

    private String[] lData = new String[2]; //loaded Data
    public TextView date, subject, sender, receiver, mailtext, from, to;
    public ProgressBar progress;
    int mailID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail_reader);
        date = (TextView) findViewById(R.id.date);
        subject = (TextView) findViewById(R.id.subject);
        sender = (TextView) findViewById(R.id.sender);
        receiver = (TextView) findViewById(R.id.receiver);
        mailtext = (TextView) findViewById(R.id.content);
        from = (TextView) findViewById(R.id.from);
        to = (TextView) findViewById(R.id.to);
        progress = (ProgressBar) findViewById(R.id.readMailProgress);

        if(getIntent().hasExtra(Constants.TAG_MAIL_ID)){
            mailID = getIntent().getIntExtra(Constants.TAG_MAIL_ID,-1);
            if(mailID == -1)
                finish();
        }else
            finish();


        if(getIntent().hasExtra(Constants.TAG_ACTIVITY_FROM)) {
                File uData = new File(getBaseContext().getFilesDir(), "/uData.txt");
                if (uData.exists()) {
                    lData = FileHandling.load(uData);
                } else {
                    if(getIntent().hasExtra(Constants.TAG_USER)) {
                        lData[0] = getIntent().getStringExtra(Constants.TAG_USER);
                        lData[1] = getIntent().getStringExtra(Constants.TAG_PW);
                    }
                }
            }

        progress.setVisibility(View.VISIBLE);
        FetchSingleMailTask fetchSingleMailTask = new FetchSingleMailTask(MailReader.this, mailID);
        fetchSingleMailTask.execute(lData[0],lData[1]);

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(MailReader.this, MailInbox.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        i.putExtra(Constants.TAG_ACTIVITY_FROM, "FromMailReader");
        startActivity(i);
        finish();
    }

}
