package pro.helms.vertretungsplannotifier;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import pro.helms.vertretungsplannotifier.task.LoginTask;
import pro.helms.vertretungsplannotifier.task.LoginTaskCompleteListener;
import pro.helms.vertretungsplannotifier.util.Constants;
import pro.helms.vertretungsplannotifier.util.DataFetcher;
import pro.helms.vertretungsplannotifier.util.FileHandling;

public class LoginAct extends AppCompatActivity implements LoginTaskCompleteListener<String> {

    public DataFetcher data;
    EditText uData, pData; //Loginform Fields
    Button loginBtn;
    ProgressBar loading;
    CheckBox remember; //RememberMe Field
    private String activityFrom, tempMailID;

    /**
     * @author Joshua Helms
     * @datetime 23.7.17
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginBtn = (Button) findViewById(R.id.login); //Widget Init
        remember = (CheckBox) findViewById(R.id.remember);
        uData = (EditText) findViewById(R.id.user);
        pData = (EditText) findViewById(R.id.pass);
        loading = (ProgressBar) findViewById(R.id.progressBar);

        if(getIntent().hasExtra(Constants.TAG_ACTIVITY_FROM)){
            activityFrom = getIntent().getStringExtra(Constants.TAG_ACTIVITY_FROM); //If activity started from PlanAct...
            if(activityFrom.equals("FromPlanAct")){                             // toast will inform user that session is expired
             Toast.makeText(this, "Sitzung abgelaufen", Toast.LENGTH_SHORT).show();
            }
            if(activityFrom.equals("FromMailFetcher")){                             // toast will inform user that session is expired
                Toast.makeText(this, "Login-Daten nicht automatisiert", Toast.LENGTH_SHORT).show();
            }
        }

        File file = new File(getBaseContext().getFilesDir(), "/uData.txt");
        if(file.exists() && !file.isDirectory()) { //Check if Login Data is already saved
            String data[] = FileHandling.load(file);
            uData.setText(data[0]);  //Then fill Textfield with it
            pData.setText(data[1]);
        }

        pData.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    loading.setVisibility(View.VISIBLE);
                    loginProc();
                    handled = true;
                }
                return handled;
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//Login Button
                loading.setVisibility(View.VISIBLE);
                loginProc();
            }
        });


    }


    private void loginProc(){
        final String user = uData.getText().toString(); //Read Login Data
        final String pass = pData.getText().toString();
        if(remember.isChecked() && !pass.equals("")){ //If Checkbox checked
            File f = new File(getApplicationContext().getFilesDir(), "/uData.txt");
            String cfgText[] = {user,pass};
            Toast.makeText(LoginAct.this, "Saved", Toast.LENGTH_SHORT).show();
            FileHandling.saveFile(f,cfgText); //App will save data for next time
        }
        LoginTask login = new LoginTask(LoginAct.this); //New Thread (AsyncTask)
        login.execute(user,pass); //Pass User Data to Login Thread
        loginBtn.setClickable(false);
    }

    @Override
    public void onLoginTaskFinish(String[] s){ //Callback Function -> called after Login Thread is finished
        if(s != null) { //when cookie with sessionId is ok, then go to plan activity
            File f = new File(getApplicationContext().getFilesDir(), "/sessionID.txt");

            FileHandling.saveFile(f, s);
            if(activityFrom.equals("FromPlan")) {
                Intent i = new Intent(LoginAct.this, PlanAct.class);
                i.putExtra(Constants.SESSION_ID, s[0]);
                i.putExtra(Constants.SESSION_PW, s[1]);
                i.putExtra(Constants.TAG_ACTIVITY_FROM, "FromLogin");
                startActivity(i);
                finish();
            }else if(activityFrom.equals("FromMailSender")){
                Intent i = new Intent(LoginAct.this, MailActivity.class);
                i.putExtra(Constants.TAG_USER, uData.getText().toString());
                i.putExtra(Constants.TAG_PW, pData.getText().toString());
                i.putExtra(Constants.TAG_ACTIVITY_FROM, "FromLogin");
                startActivity(i);
                finish();
            }else if(activityFrom.equals("FromMailFetcher")){
                Intent i = new Intent(LoginAct.this, MailInbox.class);
                i.putExtra(Constants.TAG_USER, uData.getText().toString());
                i.putExtra(Constants.TAG_PW, pData.getText().toString());
                i.putExtra(Constants.TAG_ACTIVITY_FROM, "FromLogin");
                startActivity(i);
                finish();
            }
        }else{ //else inform user that login has failed
            loginBtn.setClickable(true);
            loading.setVisibility(View.INVISIBLE);
            pData.setText("");
            Toast.makeText(this, "Login failed", Toast.LENGTH_LONG).show();
        }

    }


}

