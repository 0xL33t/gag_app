package pro.helms.vertretungsplannotifier.task;

import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;

import java.util.ArrayList;

import javax.mail.Message;

import pro.helms.vertretungsplannotifier.MailInbox;
import pro.helms.vertretungsplannotifier.MailReader;
import pro.helms.vertretungsplannotifier.util.Constants;
import pro.helms.vertretungsplannotifier.util.InboxAdapter;
import pro.helms.vertretungsplannotifier.util.MailReceiver;
import pro.helms.vertretungsplannotifier.util.SingleMail;

public class FetchMailTask extends AsyncTask<String, Object, ArrayList<SingleMail>> {
    private MailInbox mI;

    public FetchMailTask(MailInbox mI) {
        this.mI = mI;
    }

    @Override
    protected ArrayList<SingleMail> doInBackground(String... params) {
        Message[] mails;
        ArrayList<SingleMail> list = new ArrayList<>();
            try {
                mails = MailReceiver.getMails(params[0], params[1]);
                for (int i = mails.length - 1; i > mails.length - 21; i--) {
                    list.add(new SingleMail(mails[i].getFrom()[0].toString(), mails[i].getSubject(), mails[i].getFlags(), mails[i].getMessageNumber()));
                }
                return list;
            } catch (Exception e) {
                e.printStackTrace();
            }
        

        return null;
    }

    @Override
    protected void onPreExecute() {
        mI.loading.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPostExecute(ArrayList<SingleMail> result) {
        InboxAdapter curAdapter = new InboxAdapter(mI,result);
        mI.loading.setVisibility(View.INVISIBLE);
        mI.inboxAdapter = curAdapter;
        mI.mails.setAdapter(curAdapter);
        mI.mails.addFooterView(mI.loadMoreView);
        mI.mails.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SingleMail mail = (SingleMail) parent.getItemAtPosition(position);
                if(mail != null) {
                    Intent i = new Intent(mI, MailReader.class);
                    i.putExtra(Constants.TAG_USER, mI.lData[0]);
                    i.putExtra(Constants.TAG_PW, mI.lData[1]);
                    i.putExtra(Constants.TAG_MAIL_ID, mail.mailID);
                    i.putExtra(Constants.TAG_ACTIVITY_FROM, "FromMailFetcher");
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mI.getBaseContext().startActivity(i);
                    mI.finish();
                }
            }
        });

        mI.mails.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if(lastInScreen == totalItemCount){
                    FetchMoreMailTask moreMails = new FetchMoreMailTask(mI);
                    moreMails.execute(mI.lData[0],mI.lData[1]);
                }
            }
        });

    }


}
