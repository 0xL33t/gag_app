package pro.helms.vertretungsplannotifier.task;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;

import javax.mail.Message;

import pro.helms.vertretungsplannotifier.MailInbox;
import pro.helms.vertretungsplannotifier.util.MailReceiver;
import pro.helms.vertretungsplannotifier.util.SingleMail;

/**
 * Created by JoshPC on 8 Sep 2017.
 *
 * @project: VertretungsplanNotifier
 * @author: Joshua Helms
 */

class FetchMoreMailTask extends AsyncTask<String, Object, ArrayList<SingleMail>> {
    private MailInbox mI;
    private Context context;

    FetchMoreMailTask(MailInbox mI) {
        this.mI = mI;
        context = mI.getBaseContext();
    }

    protected ArrayList<SingleMail> doInBackground(String... params) {
        Message[] mails;
        ArrayList<SingleMail> list = new ArrayList<>();
        try {
            mails = MailReceiver.getMails(params[0], params[1]);
            int startPoint = mails.length - (mI.inboxAdapter.getCount() + 1);
            int mailsToLoad = 20;
            if(startPoint - 20 <= -1) {
                mailsToLoad = startPoint;
                if(startPoint == -1)
                    return null;
            }
            for (int i = startPoint; i >= startPoint -  mailsToLoad; i--) {
                list.add(new SingleMail(mails[i].getFrom()[0].toString(), mails[i].getSubject(), mails[i].getFlags(), mails[i].getMessageNumber()));
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<SingleMail> result) {
        if(result != null) {
            mI.inboxAdapter.add(result);
            mI.inboxAdapter.notifyDataSetChanged();
        }else
            mI.mails.removeFooterView(mI.loadMoreView);
    }
}
