package pro.helms.vertretungsplannotifier.task;

import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

import com.google.firebase.crash.FirebaseCrash;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import pro.helms.vertretungsplannotifier.MailActivity;
import pro.helms.vertretungsplannotifier.util.Constants;
import pro.helms.vertretungsplannotifier.util.MailSender;

/**
 * Created in VertretungsplanNotifier by JoshLaptop on 13/07/2017.
 */

public class SendMailTask extends AsyncTask {

    private MailActivity sendMailActivity;
    private View view;

    public SendMailTask(MailActivity activity, View view){ //Constructor
        sendMailActivity = activity;
        this.view = view;
    }

    @Override
    protected void onProgressUpdate(Object[] values) {
        Snackbar.make(view, values[0].toString(), Snackbar.LENGTH_LONG)
                .setAction(values[0].toString(), null).show();
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try{
            Log.i("SendMailTask", "About to instantiate Mailing...");
            publishProgress("Eingabe verarbeiten ...");
            MailSender androidMail = new MailSender(params[0].toString(),params[1].toString(),
                    (List) params[2], params[3].toString(), params[4].toString());
            publishProgress("Mail vorbereiten...");
            androidMail.createEmailMessage();
            publishProgress("Email senden ...");
            androidMail.sendEmail();
            publishProgress("Mail gesendet.");
        } catch (UnsupportedEncodingException e) {
            publishProgress(e.getMessage());
            Log.e("SendMailTask", e.getMessage(), e);
            e.printStackTrace();
            FirebaseCrash.logcat(Log.ERROR, Constants.TAG, "SendMailTask:: UnsupportedEncodingException");
            FirebaseCrash.report(e);
        } catch (AddressException e) {
            publishProgress(e.getMessage());
            Log.e("SendMailTask", e.getMessage(), e);
            e.printStackTrace();
            FirebaseCrash.logcat(Log.ERROR, Constants.TAG, "SendMailTask:: AddressException");
            FirebaseCrash.report(e);
        } catch (MessagingException e) {
            publishProgress(e.getMessage());
            Log.e("SendMailTask", e.getMessage(), e);
            e.printStackTrace();
            FirebaseCrash.logcat(Log.ERROR, Constants.TAG, "SendMailTask:: MessagingException");
            FirebaseCrash.report(e);
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        sendMailActivity.loading.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPostExecute(Object o) {
        sendMailActivity.loading.setVisibility(View.INVISIBLE);
    }
}
