package pro.helms.vertretungsplannotifier.task;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.google.firebase.crash.FirebaseCrash;

import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;

import pro.helms.vertretungsplannotifier.NotificationReceiver;
import pro.helms.vertretungsplannotifier.PlanAct;
import pro.helms.vertretungsplannotifier.util.Constants;
import pro.helms.vertretungsplannotifier.util.DataFetcher;
import pro.helms.vertretungsplannotifier.util.FileHandling;

/**
 * Created in VertretungsplanNotifier by JoshPC on 19 Jun 2017.
 */

public class PageTask extends AsyncTask<String, Object, Document> { //Thread to handle the connection to the plan

    private PlanAct planAct;
    private PageTaskCompleteListener<Document> callback; //callback method on thread finish

    public PageTask(PlanAct planAct) {
        this.planAct = planAct;
        this.callback = planAct;
    }

    public PageTask(NotificationReceiver cb) {
        this.callback = cb;
    }


    @Override
    protected Document doInBackground(String... params) {
        DataFetcher fetcher = new DataFetcher();

        Document vertretungsplan;
        try {
            vertretungsplan = fetcher.getPage(params[0], params[1]); //connect and fetch plan
        } catch (IOException e) {
            e.printStackTrace();
            FirebaseCrash.logcat(Log.ERROR, Constants.TAG, "PageTask::doInBackground(): Fetching Page Data failed");
            FirebaseCrash.report(e);
            return null;
        }

        return vertretungsplan;
    }


    @Override
    protected void onPostExecute(Document d) {
        if (callback != null) //callback with algorithm to execute when sessionID expired
            callback.onPageTaskFinish(d);

        if (!d.baseUri().contains("gymnasium-gag.de/iserv/login") && planAct != null) { //otherwise parse table
            ArrayList<String> parsedPlan;
            String[] tableToSave = null;

            parsedPlan = planAct.parseTable(d);
            tableToSave = new String[parsedPlan.size()];
            planAct.tv_plan.setText("");
            planAct.loading.setVisibility(View.INVISIBLE);
            for (int i = 0; i < parsedPlan.size(); i++) {
                planAct.tv_plan.append(parsedPlan.get(i)); //Display Text in TextView
                tableToSave[i] = parsedPlan.get(i).substring(0,parsedPlan.get(i).length()-2);
            }
            if(planAct.tv_plan.getText() == "") planAct.tv_plan.setText("Es fällt bei dir leider momentan nichts aus " + PlanAct.getEmojiByUnicode(0x1F612));

            FileHandling.saveFile(planAct.lastTable, tableToSave);
        }
    }

}
