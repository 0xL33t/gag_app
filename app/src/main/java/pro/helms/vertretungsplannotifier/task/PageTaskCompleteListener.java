package pro.helms.vertretungsplannotifier.task;

import org.jsoup.nodes.Document;

public interface PageTaskCompleteListener<T> {
    void onPageTaskFinish(Document output);
}
