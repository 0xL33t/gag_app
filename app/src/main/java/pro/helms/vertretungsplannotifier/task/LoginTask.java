package pro.helms.vertretungsplannotifier.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

import java.io.File;

import pro.helms.vertretungsplannotifier.LoginAct;
import pro.helms.vertretungsplannotifier.util.Constants;
import pro.helms.vertretungsplannotifier.util.DataFetcher;
import pro.helms.vertretungsplannotifier.util.FileHandling;

/**
 * Created in VertretungsplanNotifier by JoshLaptop on 01/07/2017.
 */
public class LoginTask extends AsyncTask<String, Object, String[]> { //Thread Class


    private LoginTaskCompleteListener<String> callback; //callback object
    private Context mContext;

    public LoginTask(LoginAct cb) {
        this.callback = cb;
        this.mContext = cb.getBaseContext();
    } //init callback to overridden method in main class

    public LoginTask(Context context) {
        this.callback = null;
        mContext = context;
    }

    @Override
    protected String[] doInBackground(String... params) { //main background task
        String[] phpSession = null;
        DataFetcher fetcher = new DataFetcher(); //new object to handle login
        try {
            phpSession = fetcher.login(params[0], params[1]); //post request by jsoup with userdata @return->cookie with sessionID needed to get plan data
        } catch (Exception e) {
            e.printStackTrace();
            FirebaseCrash.logcat(Log.ERROR, Constants.TAG, "LoginTask::doInBackground(): Login Process Error");
            FirebaseCrash.report(e);
        }
        if (phpSession != null)
            return phpSession;
        else
            return null; //return null if login has failed
    }

    @Override
    protected void onPostExecute(String[] phpSession) {
        if (callback != null) {
            callback.onLoginTaskFinish(phpSession); //fire up callback func if task has finished
        }else{  //when callback is null, the logintask is used by the NotificationReceiver
            File sessID = new File(mContext.getApplicationContext().getFilesDir(), "/sessionID.txt");
            if (phpSession != null) {
                FileHandling.saveFile(sessID, phpSession); //sessionID only saved as File and not passed
            }else{
                sessID.delete();
            }
        }
    }
}
