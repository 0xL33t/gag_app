package pro.helms.vertretungsplannotifier.task;



public interface LoginTaskCompleteListener<T> {
    void onLoginTaskFinish(String[] output);
}


