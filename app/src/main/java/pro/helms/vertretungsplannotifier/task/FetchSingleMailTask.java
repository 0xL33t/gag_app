package pro.helms.vertretungsplannotifier.task;

import android.os.AsyncTask;
import android.view.View;

import java.io.IOException;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import pro.helms.vertretungsplannotifier.MailReader;
import pro.helms.vertretungsplannotifier.util.MailReceiver;
import pro.helms.vertretungsplannotifier.util.SingleMail;

/**
 * Created by JoshPC on 7 Aug 2017.
 *
 * @project: VertretungsplanNotifier
 * @author: Joshua Helms
 */

public class FetchSingleMailTask extends AsyncTask<String,Object,SingleMail> {
    private MailReader mailReader = null;
    private int mailID = -1;

    public FetchSingleMailTask(MailReader mR, int mID){
        this.mailReader = mR;
        this.mailID = mID;
    }

    @Override
    protected SingleMail doInBackground(String[] params) {
        Message mail = null;
        try {
            mail = MailReceiver.getMail(params[0],params[1],mailID);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        if(mail != null) {
            try {
                String from = ((InternetAddress) mail.getFrom()[0]).getAddress();
                String to = "";
                if(mail.getAllRecipients() != null)
                    to = ((InternetAddress) mail.getAllRecipients()[0]).getAddress();
                return new SingleMail(from, mail.getSubject(), mail.getContent(), mail, mail.getReceivedDate(), to);
            } catch (MessagingException | IOException e) {
                e.printStackTrace();
            }
            return null;
        }else
            return null;
    }

    @Override
    protected void onPostExecute(SingleMail singleMail) {
        mailReader.date.setText(singleMail.receivedDate);
        mailReader.subject.setText(singleMail.subject);
        mailReader.sender.setText(singleMail.sender);
        mailReader.receiver.setText(singleMail.receivers);
        mailReader.mailtext.setText(singleMail.parsedContent);
        mailReader.progress.setVisibility(View.GONE);
        mailReader.to.setVisibility(View.VISIBLE);
        mailReader.from.setVisibility(View.VISIBLE);
        mailReader.date.setVisibility(View.VISIBLE);
        mailReader.subject.setVisibility(View.VISIBLE);
        mailReader.sender.setVisibility(View.VISIBLE);
        mailReader.receiver.setVisibility(View.VISIBLE);
        mailReader.mailtext.setVisibility(View.VISIBLE);
    }
}
