package pro.helms.vertretungsplannotifier.util;

/**
 * Created by JoshPC on 15 Jun 2017.
 * All const TAGS
 */

public class Constants {
    public static final String TAG_USER = "username";
    public static final String TAG_PW = "password";
    public static final String TAG = "pro.helms.vertretungsplannotifier";
    public static final String SESSION_ID = "phpSessionID";
    public static final String SESSION_PW = "phpSessionPW";
    public static final String TAG_ACTIVITY_FROM = "WhichActivity";
    public static final String TAG_MAIL_ID = "MailIDpassed";
}
