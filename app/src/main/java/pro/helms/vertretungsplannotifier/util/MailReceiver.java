package pro.helms.vertretungsplannotifier.util;

import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;

/**
 * Created by JoshPC on 23 Jul 2017 in ${PACKAGE_NAME}.
 * @project: VertretungsplanNotifier
 * @author: Joshua Helms
 */


public class MailReceiver {

    private static Properties setProperties(){
        //Creating properties for session
        Properties properties = new Properties();

        //IMAPS Protocol -> SSL IMAP
        properties.setProperty("mail.store.protocol","imaps");
        //Set host address
        properties.setProperty("mail.imaps.host","gymnasium-gag.de");
        properties.setProperty("mail.imaps.starttls.enable","true");
        //Set specified port
        properties.setProperty("mail.imaps.port","143");
        //Using SSL
        properties.setProperty("mail.imaps.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.setProperty("mail.imaps.socketFactory.fallback","false");

        return properties;
    }

    public static Message[] getMails(String user, String password) throws Exception{

        //Creating properties for session
        Properties properties = new Properties();

        //IMAPS Protocol -> SSL IMAP
        properties.setProperty("mail.store.protocol","imap");
        properties.setProperty("mail.imap.starttls.required","true");
        /*
        //Set host address
        properties.setProperty("mail.imap.host","gymnasium-gag.de");
        //Set specified port
        properties.setProperty("mail.imap.port","143");
        //Using SSL
        properties.setProperty("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.setProperty("mail.imap.socketFactory.fallback","false");
        */
        //Setting IMAPS Session
        Session imapSession = Session.getDefaultInstance(properties,null);

        //Get all Mails by IMAPS
        Store store = imapSession.getStore("imap");

        //Connect to Server by sending authentication details
        store.connect("gymnasium-gag.de",user,password);

        //Get all mails in Inbox Folder
        Folder inbox = store.getFolder("INBOX");
        inbox.open(Folder.READ_ONLY);
        return inbox.getMessages();
    }

    public static Message getMail(String user, String password, int messageID) throws MessagingException {
        //Creating properties for session
        Properties properties = new Properties();

        //IMAPS Protocol -> SSL IMAP
        properties.setProperty("mail.store.protocol","imap");
        properties.setProperty("mail.imap.starttls.required","true");
        /*
        //Set host address
        properties.setProperty("mail.imap.host","gymnasium-gag.de");
        //Set specified port
        properties.setProperty("mail.imap.port","143");
        //Using SSL
        properties.setProperty("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.setProperty("mail.imap.socketFactory.fallback","false");
        */
        //Setting IMAPS Session
        Session imapSession = Session.getDefaultInstance(properties,null);

        //Get all Mails by IMAPS
        Store store = imapSession.getStore("imap");

        //Connect to Server by sending authentication details
        store.connect("gymnasium-gag.de",user,password);
        //Get all mails in Inbox Folder
        Folder inbox = store.getFolder("INBOX");
        inbox.open(Folder.READ_WRITE);
        inbox.getMessage(messageID).setFlag(Flags.Flag.SEEN, true);
        return inbox.getMessage(messageID);
    }


}
