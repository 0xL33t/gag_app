package pro.helms.vertretungsplannotifier.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.mail.Flags;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeUtility;

/**
 * Created by JoshPC on 23 Jul 2017.
 */

public class SingleMail{
    //ListView
    public String sender;
    public String subject;
    String initials;
    boolean unseen = false;
    public int mailID = -1;
    //Mail
    public String parsedContent;
    public String receivedDate;
    public String receivers;

    private String nameExtractor(String name){
        if(name.contains(" ")) {
            StringBuilder temp = new StringBuilder();
            temp.append(name.split("<")[0]);
            temp.deleteCharAt(temp.length() - 1);
            return temp.toString();
        }else
            return name;
    }

    private String initialExtractor(String name){
        StringBuilder temp = new StringBuilder();

        if(name.contains(" ")) {
            String[] splittedNames = name.split(" ");
            temp.append(splittedNames[0].charAt(0));
            temp.append(splittedNames[1].charAt(0));
            return temp.toString();
        }else {
            return temp.append(name.charAt(0)).toString();
        }
    }

    private String getFittingLength(String target){
        if(target.length() < 35)
            return target;

        target = target.substring(0, 31);
        return target + "...";
    }

    private String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

    private String dumpPart(Part p) throws Exception {

        InputStream is = p.getInputStream();
        // If "is" is not already buffered, wrap a BufferedInputStream
        // around it.
        if (!(is instanceof BufferedInputStream)) {
            is = new BufferedInputStream(is);
        }
        return getStringFromInputStream(is);
    }

    private String getFinalContent(Part p) throws MessagingException,
            IOException {

        String finalContents = "";
        if (p.getContent() instanceof String) {
            finalContents = (String) p.getContent();
        } else {
            Multipart mp = (Multipart) p.getContent();
            if (mp.getCount() > 0) {
                Part bp = mp.getBodyPart(0);
                try {
                    finalContents = dumpPart(bp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return finalContents.trim();
    }

    private String parseContentObject (Object content,  Part messagePart) throws MessagingException, IOException {
        StringBuilder stringBuilder = new StringBuilder();

        if (content instanceof Multipart) {
            messagePart = ((Multipart) content).getBodyPart(0);
        }
        // -- Get the content type --
        String contentType = messagePart.getContentType();
        if (contentType.startsWith("TEXT/PLAIN")
                || contentType.startsWith("TEXT/HTML")) {
            stringBuilder. append(getFinalContent(messagePart));
        }
            return stringBuilder.toString();
        }

    private static Calendar toCalendar(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    public SingleMail(String sender, String subject, Flags flags, int mID) throws UnsupportedEncodingException { //ListView
        this.sender = nameExtractor(MimeUtility.decodeText(sender));
        this.subject = MimeUtility.decodeText(subject);
        this.initials = initialExtractor(this.sender).toUpperCase();
        this.mailID = mID;
        if(!flags.contains(Flags.Flag.SEEN))
            unseen = true;
    }

    public SingleMail(String sender, String subject, Object content, Part mPart, Date date, String receivers) throws IOException, MessagingException {
        Calendar calDate = toCalendar(date);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        dateFormat.setTimeZone(calDate.getTimeZone());
        this.sender = sender;
        this.subject = MimeUtility.decodeText(subject);
        this.parsedContent = parseContentObject(content, mPart);
        this.receivedDate = dateFormat.format(calDate.getTime());
        this.receivers = getFittingLength(receivers);
    }


}
