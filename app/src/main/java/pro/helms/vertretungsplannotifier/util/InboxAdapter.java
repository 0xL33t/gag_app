package pro.helms.vertretungsplannotifier.util;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import pro.helms.vertretungsplannotifier.MailInbox;
import pro.helms.vertretungsplannotifier.R;

/**
 * @author Joshua Helms
 * @datetime 23.7.17
 */


public class InboxAdapter extends BaseAdapter {
    private MailInbox mailInbox;
    private Context context;
    private ArrayList<SingleMail> list;
    private ColorGenerator generator = ColorGenerator.MATERIAL;

    public InboxAdapter(MailInbox mailInbox,ArrayList<SingleMail> list) {
        this.mailInbox = mailInbox;
        this.context = mailInbox.getBaseContext();
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        if(row==null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.single_row,parent,false);
        }
        TextView sender = (TextView) row.findViewById(R.id.sender);
        TextView subject = (TextView) row.findViewById(R.id.subject);
        ImageView initials = (ImageView) row.findViewById(R.id.initials);



        SingleMail temp = list.get(position);
        TextDrawable drawable = TextDrawable.builder().buildRect(temp.initials, generator.getColor(temp.sender));
        initials.setImageDrawable(drawable);
        sender.setText(temp.sender);
        subject.setText(temp.subject);
        if(temp.unseen) {
            subject.setTypeface(null, Typeface.BOLD);
            sender.setTypeface(null, Typeface.BOLD);
        }else{
            subject.setTypeface(null, Typeface.NORMAL);
            sender.setTypeface(null, Typeface.NORMAL);
        }
        return row;
    }

    public void add(ArrayList<SingleMail> toAdd) {
        for(SingleMail mail : toAdd)
            list.add(mail);

    }
}
