package pro.helms.vertretungsplannotifier.util;

import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;


/**
 * Created in VertretungsplanNotifier by JoshLaptop on 13/07/2017.
 * Used by AsyncTask
 */

public class MailSender {

    final String emailPort = "587"; //i-serv smtp port
    final String smtpAuth = "true";
    final String starttls = "true";
    final String emailHost = "gymnasium-gag.de";

    String fromEmail; //Auth
    String fromPassword;
    List<String> toEmailList;
    String emailSubject;
    String emailBody;

    Properties emailProperties;
    Session mailSession;
    MimeMessage emailMessage;

    public MailSender() {

    }

    public MailSender(String fromEmail, String fromPassword,
                      List toEmailList, String emailSubject, String emailBody){

        this.fromEmail = fromEmail;
        this.fromPassword = fromPassword;
        this.toEmailList = toEmailList;
        this.emailSubject = emailSubject;
        this.emailBody = emailBody;

        emailProperties = System.getProperties();
        emailProperties.put("mail.smtp.port", emailPort);
        emailProperties.put("mail.smtp.auth", smtpAuth);
        emailProperties.put("mail.smtp.starttls.enable",starttls);
        Log.i("MAIL","Mail server properties set");
    }

    public MimeMessage createEmailMessage() throws AddressException, MessagingException, UnsupportedEncodingException {
        mailSession = Session.getDefaultInstance(emailProperties, null);
        emailMessage = new MimeMessage(mailSession);

        emailMessage.setFrom(new InternetAddress(fromEmail + "@gymnasium-gag.de",fromEmail + "@gymnasium-gag.de"));
        for (String toEmail : toEmailList) {
            emailMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
            Log.i("MAIL", "toEmail:" + toEmail);
        }

        emailMessage.setSubject(emailSubject);
        emailMessage.setContent(emailBody, "text/html");
        Log.i("MAIL","Email Message created.");
        return emailMessage;
    }

    public void sendEmail() throws AddressException, MessagingException{

        Transport transport = mailSession.getTransport("smtp");
        transport.connect(emailHost, fromEmail, fromPassword);
        Log.i("MAIL","allrecipients: " + emailMessage.getAllRecipients());
        transport.sendMessage(emailMessage, emailMessage.getAllRecipients());
        transport.close();
        Log.i("MAIL","Mail was sent successfully");
    }
}
