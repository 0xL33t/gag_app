package pro.helms.vertretungsplannotifier.util;

import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Joshua Helms
 * @datetime 23.7.17
 */

public class FileHandling {

    public static void saveFile(File file, String data[]){
        FileOutputStream fos = null;

        try{
            fos = new FileOutputStream(file);
        }catch (FileNotFoundException e){
            e.printStackTrace();
            FirebaseCrash.logcat(Log.ERROR, Constants.TAG, "FileHandling::saveFile(): File not found");
            FirebaseCrash.report(e);
        }

        try{
            try{
                for(int i = 0; i < data.length; i++){
                    assert fos != null;
                    fos.write(data[i].getBytes());
                    if(i < data.length - 1){
                        fos.write("\n".getBytes());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                FirebaseCrash.logcat(Log.ERROR, Constants.TAG, "FileHandling::saveFile(): Can't write Data to File");
                FirebaseCrash.report(e);
            }
        }finally {
            try{
                assert fos != null;
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
                FirebaseCrash.logcat(Log.ERROR, Constants.TAG, "FileHandling::saveFile(): saveFile(): Can't close File");
                FirebaseCrash.report(e);
            }
        }
    }

    public static String[] load(File file){
        FileInputStream fis = null;
        try{
            fis = new FileInputStream(file);
        }catch (FileNotFoundException e){
            e.printStackTrace();
            FirebaseCrash.logcat(Log.ERROR, Constants.TAG, "FileHandling::load(): File not found");
            FirebaseCrash.report(e);
        }

        assert fis != null;
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isr);

        String temp;
        int count=0;
        try
        {
            while ((temp=br.readLine()) != null)
            {
                count++;
            }
        }
        catch (IOException e) {
            e.printStackTrace();
            FirebaseCrash.logcat(Log.ERROR, Constants.TAG, "FileHandling::load(): Can't read data");
            FirebaseCrash.report(e);
        }


        try{
            fis.getChannel().position(0);
        }catch(IOException e){
            e.printStackTrace();
            FirebaseCrash.logcat(Log.ERROR, Constants.TAG, "FileHandling::load(): Can't read data");
            FirebaseCrash.report(e);
        }

        String array[] = new String[count];

        String line;
        int i = 0;
        try{
            while((line=br.readLine()) != null){
                array[i] = line;
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
            FirebaseCrash.logcat(Log.ERROR, Constants.TAG, "FileHandling::load(): Can't read data");
            FirebaseCrash.report(e);
        }
        return array;
    }

}
