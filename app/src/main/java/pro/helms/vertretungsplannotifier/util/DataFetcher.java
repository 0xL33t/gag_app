package pro.helms.vertretungsplannotifier.util;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * @author Joshua Helms
 * @datetime 23.7.17
 */

public class DataFetcher { //class to handle login request and retrieve html table




    public String[] login(String user, String pass) throws Exception { //login method

        Connection.Response mRes; //Connection Handler
        String sessionID, sessionPW; //sessionID required later

        mRes = Jsoup
                .connect("https://gymnasium-gag.de/iserv/login_check")
                .method(Connection.Method.POST) //POST Request
                .data("_username", user)
                .data("_password", pass)
                .execute();


        sessionID = mRes.cookie("PHPSESSID"); //get sessionID from retrieved cookies
        sessionPW = mRes.cookie("PHPSESSPW");//get sessionPW from retrieved cookies

        if (sessionID == null || sessionPW == null) {
            return null;
        }else{
            return new String[] {sessionID,sessionPW};
        }

    }

     public Document getPage(String sessionID, String sessionPW) throws IOException { //get html table in a doc to parse later

         Document doc = Jsoup.connect("https://gymnasium-gag.de/iserv/plan/show/raw/Vertretung_Jg12/")
                    .cookie("PHPSESSID", sessionID)
                    .cookie("PHPSESSPW",sessionPW)
                    .get();




        return doc;


    }
}
