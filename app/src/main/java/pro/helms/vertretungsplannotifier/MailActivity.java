package pro.helms.vertretungsplannotifier;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.ProgressBar;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import pro.helms.vertretungsplannotifier.task.SendMailTask;
import pro.helms.vertretungsplannotifier.util.Constants;
import pro.helms.vertretungsplannotifier.util.FileHandling;

    /**
       * @author Joshua Helms
       * @datetime 23.7.17
     */

public class MailActivity extends AppCompatActivity {
    public ProgressBar loading;
    Toolbar toolbar;
    EditText email,subject;
    MultiAutoCompleteTextView body;
    FloatingActionButton fab;
    File fuData;
    String uData[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        loading = (ProgressBar) findViewById(R.id.loading);
        email = (EditText) findViewById(R.id.emailAdress);
        subject = (EditText) findViewById(R.id.subject);
        body = (MultiAutoCompleteTextView) findViewById(R.id.messageBody);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        fuData = new File(getBaseContext().getFilesDir(),"/uData.txt");

        if(getIntent().hasExtra(Constants.TAG_ACTIVITY_FROM)){
            String activityFrom = getIntent().getStringExtra(Constants.TAG_ACTIVITY_FROM);
            if(activityFrom.equals("FromLogin")){
                uData = new String[2];
                uData[0] = getIntent().getStringExtra(Constants.TAG_USER);
                uData[1] = getIntent().getStringExtra(Constants.TAG_PW);
            }else{
                if(fuData.exists() && !fuData.isDirectory()){
                    uData = FileHandling.load(fuData);
                }else{
                    Intent i = new Intent(MailActivity.this,LoginAct.class);
                    i.putExtra(Constants.TAG_ACTIVITY_FROM,"FromMailSender");
                    startActivity(i);
                    finish();
                }
            }
            }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mailLogin = uData[0];
                String mailPW = uData[1];
                String toEmails = email.getText().toString();
                List toEmailList = Arrays.asList(toEmails.split("//s*//s*"));
                String emailSubject = subject.getText().toString();
                String emailBody = body.getText().toString();

                new SendMailTask(MailActivity.this,view).execute(mailLogin,mailPW,
                        toEmailList,emailSubject, emailBody);
            }
        });
    }

}
