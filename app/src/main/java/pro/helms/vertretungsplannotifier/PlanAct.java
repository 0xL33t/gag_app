package pro.helms.vertretungsplannotifier;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.util.ArrayList;

import pro.helms.vertretungsplannotifier.task.PageTask;
import pro.helms.vertretungsplannotifier.task.PageTaskCompleteListener;
import pro.helms.vertretungsplannotifier.util.Constants;
import pro.helms.vertretungsplannotifier.util.FileHandling;

/**
 * @author Joshua Helms
 * @datetime 23.7.17
 */

public class PlanAct extends AppCompatActivity implements PageTaskCompleteListener<Document> {
    String sessionID = null, sessionPW = null;
    public TextView tv_plan;
    public ProgressBar loading;
    public File lastTable;
    private File kList, sID, uData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan);

        ImageButton refresh = (ImageButton) findViewById(R.id.refresh); //init widgets
        tv_plan = (TextView) findViewById(R.id.plan);
        loading = (ProgressBar) findViewById(R.id.loading);


        boolean isConnected = NotificationReceiver.isNetworkAvailable(getApplicationContext());

        String activityFrom = getIntent().getStringExtra(Constants.TAG_ACTIVITY_FROM); //check which activity started this one

        sID = new File(getBaseContext().getFilesDir(), "/sessionID.txt");
        lastTable = new File(getBaseContext().getFilesDir(), "/lastTable.txt");



            if (activityFrom.equals("FromMain")) {
                boolean isIDInvalid = true;
                if (sID.exists() && !sID.isDirectory()) { //check if sessionID is existing
                    String tmp[] = FileHandling.load(sID);
                    if (tmp.length > 1) {
                        sessionID = tmp[0];
                        sessionPW = tmp[1];
                        isIDInvalid = false;
                        if(isConnected) {
                            loading.setVisibility(View.VISIBLE);
                            PageTask vPlan = new PageTask(this);
                            vPlan.execute(sessionID, sessionPW);
                        }else
                            tv_plan.setText("Kein Internet verfügbar " + getEmojiByUnicode(0x1F622));
                    }
                }
                if (isIDInvalid){ //otherwise repeat login

                    Intent i = new Intent(PlanAct.this, LoginAct.class);
                    i.putExtra(Constants.TAG_ACTIVITY_FROM,"FromPlan");
                    startActivity(i);
                    finish();
                }
            } else if (activityFrom.equals("FromLogin")) { //After sucessfull login fetch plan data instantly
                sessionID = getIntent().getStringExtra(Constants.SESSION_ID);
                sessionPW = getIntent().getStringExtra(Constants.SESSION_PW);
                if(isConnected) {
                    loading.setVisibility(View.VISIBLE);
                    PageTask vPlan = new PageTask(this);
                    vPlan.execute(sessionID, sessionPW);
                }else
                    tv_plan.setText("Kein Internet verfügbar " + getEmojiByUnicode(0x1F622));
            }


        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //button to refresh
                if (NotificationReceiver.isNetworkAvailable(getBaseContext())) {
                    tv_plan.setText("");
                    loading.setVisibility(View.VISIBLE);
                    PageTask vPlan = new PageTask(PlanAct.this);
                    vPlan.execute(sessionID, sessionPW);
                } else {
                    tv_plan.setText("Kein Internet verfügbar " + getEmojiByUnicode(0x1F622));
                    Toast.makeText(PlanAct.this, "No Internet available", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void onPageTaskFinish(Document d) { //callback method when retrieving html data is finished
        if (d.baseUri().contains("gymnasium-gag.de/iserv/login")) { //if sessionID expired, do a new login
            Intent i = new Intent(PlanAct.this, LoginAct.class);
            i.putExtra(Constants.TAG_ACTIVITY_FROM, "FromPlan");
            startActivity(i);
            finish();
        }
    }

    public ArrayList<String> parseTable(Document plan) { //helper method to parse retrieved html table

        kList = new File(getApplicationContext().getFilesDir(), "/kurzel.txt");
        ArrayList<String> entfall = new ArrayList<>();
        String lehrerK[];
        if (kList.exists() && !kList.isDirectory()) {
            lehrerK = FileHandling.load(kList);
        } else {
            entfall.add("Bitte lege die Lehrer in den Einstellungen fest, für die du informiert werden möchtest");
            return entfall;
        }


        Element table = plan.select("table").get(1); //get html table
        Elements rows = table.select("tr"); //get all table rows

        for (int i = 1; i < rows.size(); i++) { //loop through and check if any teachers are affected
            Element row = rows.get(i);
            Elements cols = row.select("td");
            for (String aLehrerK : lehrerK) {
                if (cols.get(2).text().contains(aLehrerK)) {
                    if (cols.get(6).text().contains("Entfall"))
                        entfall.add(aLehrerK + " fällt am " + cols.get(0).text() + " in der " + cols.get(1).text() + " Stunde aus.\n\n");
                    if (cols.get(6).text().contains("Raumwechsel"))
                        entfall.add("Der Unterricht von " + aLehrerK + " in der " + cols.get(1).text() + " Stunde, findet am " + cols.get(0).text() + " im Raum " + cols.get(5).text() + " statt.\n\n");
                }
            }
        }
        return entfall; //return ArrayList with results
    }

    public static String getEmojiByUnicode(int unicode) {  //Smiley Helper :)
        return new String(Character.toChars(unicode));
    }
}


